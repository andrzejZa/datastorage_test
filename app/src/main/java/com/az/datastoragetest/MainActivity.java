package com.az.datastoragetest;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ListView list = (ListView)findViewById(R.id.list_view);
        final DataRepository repository = DataRepository.Get(this);
        final DataItemAdapter adapter=new DataItemAdapter(this,repository.Get());
        list.setAdapter(adapter);

        final EditText name_txt = findViewById(R.id.name_txt);
        final EditText email_txt = findViewById(R.id.email_txt);
        final EditText phone_txt = findViewById(R.id.phone_txt);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               DataItem item = new DataItem(name_txt.getText().toString(),phone_txt.getText().toString(), email_txt.getText().toString());
                repository.insert(item);
                adapter.notifyDataSetChanged();
                name_txt.setText("");
                email_txt.setText("");
                phone_txt.setText("");
            }
        });
        final CheckBox checkox_cb = findViewById(R.id.bg_checkbox);
        checkox_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                pref.edit().putBoolean("checkbox_on",isChecked).apply();
                setBackgroundColor(isChecked);
            }
        });
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean checkbox_on = pref.getBoolean("checkbox_on", false);
        checkox_cb.setChecked(checkbox_on);
        setBackgroundColor(checkbox_on);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setBackgroundColor(boolean checked){
        ConstraintLayout layout = findViewById(R.id.main_layout);
        if (checked)
            layout.setBackgroundColor(Color.parseColor("#BBDEFB"));
        else
            layout.setBackgroundColor(Color.parseColor("#ffffff"));
    }
}
