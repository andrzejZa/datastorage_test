package com.az.datastoragetest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.List;

/**
 * Created by stud on 08.04.2018.
 */

public class DataItemAdapter  extends BaseAdapter {
    private Context context;
    private List<DataItem> items;

    public DataItemAdapter(Context context, List<DataItem> items)
    {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return items.size();
    }

    @Override
    public Object getItem(int item) {
        // TODO Auto-generated method stub
        return items.get(item);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        DataItem entry = items.get(position);

        if(convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_row, null);
        }

        TextView name = (TextView)convertView.findViewById(R.id.row_name);
        TextView phone = (TextView)convertView.findViewById(R.id.row_phone);
        TextView email = (TextView)convertView.findViewById(R.id.row_email);

        name.setText(entry.Name);
        phone.setText(entry.Phone);
        email.setText(entry.Email);


        return convertView;
    }

}
