package com.az.datastoragetest;

/**
 * Created by stud on 08.04.2018.
 */

public class DataItem {
    String Name;
    String Phone;
    String Email;

    public DataItem(String name, String phone, String email) {
        Name = name;
        Phone = phone;
        Email = email;
    }
}
