package com.az.datastoragetest;

import android.content.Context;

import java.util.List;

/**
 * Created by stud on 08.04.2018.
 */

public class DataRepository {
    private List<DataItem> items;
    private Context context;
    private DBAdapter db;
    public List<DataItem> Get(){
        return items;
    }
    public boolean insert (DataItem item){
        items.add(item);
        db.open();
        db.InsertDataItem(item);
        db.close();
        return true;
    }

    private DataRepository(Context context){
        this.context = context;
        db = new DBAdapter(this.context);
        db.open();
        items = db.GetAllItems();
        db.close();

    }
    private static  DataRepository instance;
    public static  DataRepository Get(Context context)
    {
        if (instance == null){
            instance = new DataRepository(context);
        }
        return instance;

    }

}
